package tallerdeprogramacion;


public class PistaAterrizaje implements Comparable<PistaAterrizaje>
{
    private int numero;
    private boolean ocupada;
    private Avion v;
    public PistaAterrizaje(int numero,int capacidad){
        this.numero=numero;
        ocupada=false;
    }
    public int getNumero(){
        return numero;
    }
    public void setNumero(int nuevo){
        numero=nuevo;
    }
    public boolean esOcupada(){
        return ocupada;
    }
    public void ocupar(Avion v2){
        ocupada=true;
        v=v2;
    }
    public Avion desocupar(){
        ocupada=false;
        Avion k=v;
        v=null;
        return k;
    }
    public int compareTo(PistaAterrizaje pist){
        int res=0;
        if(pist.esOcupada() && !ocupada){
            res=-1;
        }
        if(!pist.esOcupada() && ocupada){
            res=1;
        }
        return res;
    }
}
