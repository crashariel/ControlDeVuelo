package tallerdeprogramacion;

import java.io.*;
import java.util.*;
public class Escritor
{
    private ArrayList<Avion> aviones;
    private ArrayList<Aeropuerto> aeropuertos;
    public Escritor(String archivo, ArrayList<Avion> aviones, ArrayList<Aeropuerto> aeropuertos)
    {
        this.aviones = aviones;
        this.aeropuertos = aeropuertos;
        File f;
        FileWriter fw;
        BufferedWriter bw;
        PrintWriter pw;
        try{
            f = new File(archivo+".xml");
            fw = new FileWriter(f);
            bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);
            write(bw, pw);
            pw.close();
            bw.close();
        }
        catch(Exception e){
        }
    }    
    public void write(BufferedWriter bw, PrintWriter pw)
    {
        pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<!DOCTYPE ControladorDeVuelo SYSTEM \" Comilla dobleControlador De Vuelo.dtd\" Comilla doble>\r\n<ControladorDeVuelo>\r\n");
        for(int i=0; i<aviones.size(); i++){
            pw.write("\t<Avion>\r\n");
            pw.write("\t\t<codigo>"+aviones.get(i).getCodigoAvion()+"</codigo>\r\n");              
            pw.write("\t\t<modelo>"+aviones.get(i).getModelo()+"</modelo>\r\n"); 
            pw.write("\t\t<x>"+Integer.toString(aviones.get(i).getX())+"</x>\r\n");
            pw.write("\t\t<y>"+Integer.toString(aviones.get(i).getY())+"</y>\r\n");
            pw.write("\t</Avion>\r\n");
            }
        for(int i=0; i<aeropuertos.size(); i++){
             pw.write("\t<Avion>\r\n");
             pw.write("\t\t<nombre>"+aeropuertos.get(i).getNombre()+"</nombre>\r\n");                                  
             pw.write("\t\t<cantidadPistas>"+Integer.toString(aeropuertos.get(i).getCantidadPistas())+"</cantidadPistas>\r\n");
             pw.write("\t\t<posX>"+Integer.toString(aeropuertos.get(i).getX())+"</posX>\r\n");
             pw.write("\t\t<posY>"+Integer.toString(aeropuertos.get(i).getY())+"</posY>\r\n");
             pw.write("\t</Avion>\r\n");
            }
        pw.write("</ControladorDeVuelo>");
    }
}
