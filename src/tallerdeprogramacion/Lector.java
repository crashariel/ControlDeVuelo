package tallerdeprogramacion;

import java.util.*;
import java.net.URL;
import java.io.*;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
public class Lector
{
    ArrayList<Avion> aviones;
    ArrayList<Aeropuerto> aeropuertos;
    public Lector(String archivo) 
    {
        aviones=new ArrayList<Avion>();
        aeropuertos=new ArrayList<Aeropuerto>();
        try {
            DOMParser parser = new DOMParser();
            File file = new File(archivo + ".xml");
            URL url = file.toURL();
            parser.setErrorStream(System.err);
            parser.setValidationMode(DOMParser.DTD_VALIDATION);
            parser.showWarnings(true);
            parser.parse(url);
            XMLDocument doc = parser.getDocument();
            read(doc);
        } catch (Exception e) {
            //System.out.println(e.toString());
        }
    }
    public void read(Document doc) 
    {
        NodeList nodelist = doc.getElementsByTagName("*");
        Node n;
        for (int i = 0; i < nodelist.getLength(); i++) {
            n = nodelist.item(i);
            if (n.getNodeName() == "Avion") {
                aviones.add(new Avion(nodelist.item(i + 1).getFirstChild().getNodeValue(), nodelist.item(i + 2).getFirstChild().getNodeValue(),
                        Integer.parseInt(nodelist.item(i + 3).getFirstChild().getNodeValue()), Integer.parseInt(nodelist.item(i + 4).getFirstChild().getNodeValue())));
                i += 4;
            }
            if (n.getNodeName() == "Aeropuerto") {
                aeropuertos.add(new Aeropuerto(nodelist.item(i + 1).getFirstChild().getNodeValue(), Integer.parseInt(nodelist.item(i + 2).getFirstChild().getNodeValue()),
                        Integer.parseInt(nodelist.item(i + 1).getFirstChild().getNodeValue()), Integer.parseInt(nodelist.item(i + 2).getFirstChild().getNodeValue())));
                i += 4;
            }
        }
    }
}
