package tallerdeprogramacion;

import java.util.*;

public class Diccionario {

    private static ArrayList<String> nombres = new ArrayList<String>();
    private static ArrayList<String> apellidos = new ArrayList<String>();

    private static void anadirN(String k) {
        nombres.add(k);
    }

    private static void anadirA(String k) {
        apellidos.add(k);
    }

    public static String nombre() {
        if (nombres.isEmpty()) {
            llenar();
        }
        int g = (int) (Math.random() * (nombres.size() - 1)) + 1;
        return nombres.get(g);
    }

    public static String apellido() {
        if (apellidos.isEmpty()) {
            llenar();
        }
        int g = (int) (Math.random() * (apellidos.size() - 1)) + 1;
        return apellidos.get(g);
    }

    public static void llenar() {
        String[] nom = {"Lucas", "Marco", "Luis", "Pepe", "Ayrton", "Manuel", "Samuel", "Ariel", "Daniel", "Gabriel", "Martha", "Maria", "Noemi", "Carla", "Paola", "Paula", "Mariana", "Emilio", "Enrique",
            "Isabel", "Carlos", "Carolina", "Alejandra", "Amanda", "Berta", "Raquel"};
        String[] ap = {"Ordonez", "Rodriguez", "Gutierrez", "Salamanca", "Dueri", "Castro", "Mendieta",
            "Mendez", "Smith", "Tapia", "Torrico", "Ledezma", "Flores", "Rios", "Cardenas", "Rosales", "Perez", "Pedrazas", "Lopez", "Mamani", "Morale s", "Lopera", "Monteagudo", "Montes", "Gonzales", "Lozada",
            "Reyes", "Daniels", "Vivaldi", "Vargas", "Terceros", "Vergara", "Tames", "Ergueta", "Munzon", "Puma",
            "Duran", "Waltz", "Posnansky", "Jaimes", "Torrez", "Torres", "Cabrera", "Montano", "Montan", "Pacheco",
            "Garcia", "Choque"};
        nombres.addAll(Arrays.asList(nom));
        apellidos.addAll(Arrays.asList(ap));
    }
}
