package tallerdeprogramacion;

import java.util.*;
public class Vuelo
{
    private String origen,destino;
    private ArrayList<Pasajero> pasajeros;
    private String idVuelo;
    private int tiempoVuelo=0;
    public Vuelo(String origen, String destino){
        this.origen=origen;
        this.destino=destino;
        pasajeros=new ArrayList<>();
        cargarVuelo();
    }
    private void cargarVuelo(){
        int n=(int)(Math.random()*200)+1;
        for(int i=0;i<n;i++){
            pasajeros.add(new Pasajero());
        }
    }
    private void anadir(String nombre,int Ci){
        pasajeros.add(new Pasajero(nombre,Ci));
    }
    public void tiempoAumentar(){
        tiempoVuelo++;
    }
    public String getOrigen(){
        return origen;
    }
    public String getDestino(){
        return destino;
    }  
    public String getIdVuelo(){
        return destino;
    }   
    public int getTiempoVuelo(){
        return tiempoVuelo;
    }
}