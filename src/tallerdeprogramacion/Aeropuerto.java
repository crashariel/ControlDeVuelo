package tallerdeprogramacion;

import java.util.*;

public class Aeropuerto {

    private PriorityQueue<PistaAterrizaje> pistas = new PriorityQueue<>();
    private Queue<PistaAterrizaje> cola = new LinkedList<>();
    private int cantidadPistas;
    private int x, y;
    private String nombre;

    public Aeropuerto(String nombre, int cantidadPistas, int x, int y) {
        this.cantidadPistas = cantidadPistas;
        while (cantidadPistas != 0) {
            PistaAterrizaje p = new PistaAterrizaje(cantidadPistas, (int) Math.random() * 100 + 1);
            pistas.add(p);
            cantidadPistas--;
        }
        this.x = x;
        this.nombre = nombre;
        this.y = y;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean enCola() {
        return !cola.isEmpty();
    }

    public Avion volar(int xd, int yd) {
        PistaAterrizaje pis = cola.poll();
        Avion v = pis.desocupar();
        v.setDest(xd, yd);
        return v;
    }

    public boolean anadirAvion(String codigoAvion, String modelo) {
        boolean res = false;
        Avion v = new Avion(codigoAvion, modelo, x, y);
        PistaAterrizaje aux = pistas.peek();
        if (!aux.esOcupada()) {
            aux.ocupar(v);
            pistas.poll();
            cola.offer(aux);
            pistas.add(aux);
            res = true;
        }
        return res;
    }

    public int getCantidadPistas() {
        return cantidadPistas;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
