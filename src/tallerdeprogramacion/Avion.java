package tallerdeprogramacion;

 

import java.util.*;

public class Avion {

    private String codigoAvion;
    private String modelo;
    private BarrilCombustible barril;
    private int velocidad;
    private int altura;
    private int xO, yO;
    private int xD, yD;

    public Avion(String codigoAvion, String modelo, int x, int y) {
        this.codigoAvion = codigoAvion;
        this.modelo = modelo;
        barril = new BarrilCombustible(5000);
        velocidad = 0;
        altura = y;
        xD = x;
        yD = y;
        xO = x;
        yO = y;
    }

    public void set(int a, int b) {
        xO = a;
        yO = b;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public void setDest(int a, int b) {
        xD = a;
        yD = b;
    }

    public void consumir() {
        barril.reducir();
    }

    public void llenarBarril() {
        barril.llenar();
    }

    public BarrilCombustible getBarril() {
        return barril;
    }

    public String getModelo() {
        return modelo;
    }

    public int getAltura() {
        return altura;
    }

    public void aumentarVel(int razon) {
        velocidad += razon;
    }

    public void reducirVel(int razon) {
        velocidad -= razon;
    }

    public void aumentarAltura(int razon) {
        altura += razon;
    }

    public void reducirAltura(int razon) {
        altura -= razon;
    }

    public String toString() {
        return "Velocidad: " + velocidad + "\n Altura: " + altura;
    }

    public String getCodigoAvion() {
        return codigoAvion;
    }

    public int getCombustible() {
        return barril.getCombustible();
    }

    public int getVelocidad() {
        return velocidad;
    }

    public int getX() {
        return xO;
    }

    public int getY() {
        return yO;
    }
}
