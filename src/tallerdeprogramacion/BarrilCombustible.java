package tallerdeprogramacion;


public class BarrilCombustible
{
    private int capacidadMaxima,combustible;
    public BarrilCombustible(int capacidadMaxima){
        this.capacidadMaxima=capacidadMaxima;
    }
    public void llenar(){
        combustible=capacidadMaxima;
    }
    public int getCombustible(){
        return combustible;
    }
    public void reducir(){
        combustible--;
    }
}
