package tallerdeprogramacion;

import java.util.*;

public class Mapa {

    private int ancho, altura;
    private HashMap<String, Aeropuerto> destinos;
    private ArrayList<Avion> envuelo;
    private ArrayList<Vuelo> envueloInfo;

    public Mapa(int anch, int altur) {
        ancho = anch;
        altura = altur;
        envuelo = new ArrayList<Avion>();
        envueloInfo = new ArrayList<Vuelo>();
        destinos = new HashMap<String, Aeropuerto>();
    }
    
    public Mapa() {
        envuelo = new ArrayList<Avion>();
        envueloInfo = new ArrayList<Vuelo>();
        destinos = new HashMap<String, Aeropuerto>();
    }
    
    public void anadir(String nombre, int pistas, int x, int y) {
        if (valido(x, y)) {
            destinos.put(juntar(x, y), new Aeropuerto(nombre, pistas, x, y));
        }
    }

    private boolean valido(int a, int b) {
        return a >= 0 && b >= 0 && a <= ancho && b <= altura;
    }

    private String juntar(int a, int b) {
        return a + " " + b;
    }

    public boolean anadirAvion(String codigoAvion, String modelo, int x, int y) {
        boolean res = false;
        if (valido(x, y)) {
            String k = juntar(x, y);
            if (existe(k)) {
                Aeropuerto aero = destinos.get(k);
                res = aero.anadirAvion(codigoAvion, modelo);
            }
        }
        return res;
    }

    private boolean existe(String buscar) {
        return destinos.containsKey(buscar);
    }

    public boolean volar(int xo, int yo, int xd, int yd) {
        boolean resu = false;
        if (valido(xo, yo) && valido(xd, yd)) {
            Avion res = null;
            String pat = juntar(xo, yo);
            Aeropuerto aero = buscar(pat);
            if (aero.enCola()) {
                resu = true;
                res = aero.volar(xd, yd);
                pat = juntar(xd, yd);
                Aeropuerto aux = buscar(pat);
                info(aero, aux);
            }
            envuelo.add(res);
        }
        return resu;
    }

    private void info(Aeropuerto aero, Aeropuerto aux) {
        if (aux == null) {
            envueloInfo.add(new Vuelo(aero.getNombre(), "Desconocido"));
        } else {
            envueloInfo.add(new Vuelo(aero.getNombre(), aux.getNombre()));
        }
    }

    private Aeropuerto buscar(String pat) {
        Aeropuerto aero = null;
        if (existe(pat)) {
            aero = destinos.get(pat);
        }
        return aero;
    }

    public void actualizar() {
        for (int i = 0; i < envuelo.size(); i++) {
            Avion v = envuelo.get(i);
            System.out.println(v.toString());
        }
    }
}
