/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerdeprogramacion.views;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Ariel
 */
public class NoEstaticos extends Graficador {

    private int accion;
    private String codigo;

    public NoEstaticos(Radar applet, int tamano, int naptime, Color color, String tipo) {
        super(applet, 10, 1000, color, tipo);
    }

    public int getAccion() {
        return accion;
    }

    public void sigAccion() {
        accion++;
    }

    public void run() {
        thread.setPriority(Thread.MAX_PRIORITY);
        while (thread != null) {
            switch (getAccion()) {
                case 1:
                    aumentarAltura(1);
                    setDistancia(2);
                    if (getAltura() == 10) {
                        sigAccion();
                    }
                case 2:
                    setDistancia(2);
                    if (getDistancia() < 16) {
                        sigAccion();
                    }
                case 3:
                    reducirAltura(1);
                    setDistancia(2);
            }
            if (ox < desX) {
                ox = ox + 1;
                if ((desY - oy != 0)) {
                    oy = (int) formula.devolverY(ox);
                }
                try {
                    thread.sleep(naptime);
                } catch (InterruptedException e) {
                }
            } else {
                ox = ox - 1;
                if ((desY - oy != 0)) {
                    oy = (int) formula.devolverY(ox);
                }
                try {
                    thread.sleep(naptime);
                } catch (InterruptedException e) {
                }
            }
            applet.repaint();
        }
    }

    @Override
    public void paint(Graphics graphic) {
        graphic.setColor(color);
        graphic.fillOval(ox, oy, tamano, tamano);
        graphic.drawRect(ox, oy + tamano, toString().length(), tamano);

        if (!(getOy() == getDesY())) {

            graphic.drawString(toString(), ox, oy + tamano + tamano);
        }
        if (coorEscala.isEmpty()) {
            if (getOy() == getDesY()) {
                graphic.setColor(Color.white);
                graphic.fillOval(ox, oy, tamano, tamano);
                graphic.drawRect(ox, oy + tamano, toString().length(), tamano);
            }
        } else {
            int x = 0;
            int y = 0;
            for (Integer i : coorEscala.values()) {
                y = coorEscala.get(1);
                break;
            }
            
            for (Integer key : coorEscala.keySet()) {
                x = key;
                break;
            }
            coorEscala.remove(x);
            modificacion(x, y);
        }
    }

    private void modificacion(int x, int y) {
        setDest(x, y);
    }

    @Override
    public String toString() {
        return getTipo() + "  altura  " + getAltura();
    }
    
    @Override
    public void setDest(int x, int y) {
        desX = x;
        desY = y;
    }

}
