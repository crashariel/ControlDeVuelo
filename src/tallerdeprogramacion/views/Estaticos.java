/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerdeprogramacion.views;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Ariel
 */
public class Estaticos extends Graficador {



    public Estaticos(Radar applet, int tamano, int naptime, Color color, String tipo) {
        super(applet, 10, 1000, color, tipo);
    }

   @Override
    public void paint(Graphics graphic) {
       if (getOy() == getDesY()) {
        graphic.setColor(color);
        graphic.fillOval(ox, oy, tamano, tamano);
        graphic.drawRect(ox, oy + tamano, toString().length(), tamano);
        graphic.drawString(toString(), ox, oy + tamano + tamano);
       }
    }

    @Override
    public String toString() {
          return getTipo();
    }
}
