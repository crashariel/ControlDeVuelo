/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerdeprogramacion.views;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Ariel
 */
public class ControlDeVuelo extends javax.swing.JFrame {

    private Radar radar;

    public ControlDeVuelo() {
        radar = new Radar();
        radar.init();
    }

    public ControlDeVuelo(Radar radar) {
        this.radar = radar;
    }

    public void pantallaso() {
        Frame frame = new Frame("Trafico Aereo");
        frame.add(radar);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(new Dimension(758, 415));
        frame.setMinimumSize(new Dimension(758, 415));
        frame.setMaximumSize(new Dimension(758, 415));
        frame.setVisible(true);
        frame.addWindowListener(
                new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        System.exit(0);
                    }

                    @Override
                    public void windowClosing(WindowEvent e) {
                        windowClosed(e);
                    }
                }
        );
    }

    public void anadir(String tipo, String color, int x, int y, int desX, int desY) {
        if (x == desX && y == desY) {
            radar.anadir(x, y, color, tipo);
        }else {
        radar.anadir(x, y, desX, desY, color, tipo);
        }

    }
    
    public void colocar(String tipo, String color, int x, int y) {
        radar.anadir(x, y, color, tipo);
    }

    public void start() {
        radar.start();
    }
    
    public void detener(){
        radar.stop();
    }
    
    
    public void darEscala(String asignar, int x, int y) {
        for (Graficador buscar : radar.getLista()) {
            if (buscar.getTipo().equals(asignar)) {
                buscar.darEscala(x, y);
                
            }
        }
    }

    private Graficador darAvion(int inidice) {
        for (Graficador buscar : radar.getLista()) {
            if (buscar.getTipo().equals("Avion")) {
                return buscar;
            }
        }
        return null;
    }

    public int getCantidadAviones() {
        return radar.getCantidadAvion();
    }

    public int getCantidadAeropuertos() {
        return radar.getCantidadAeropuerto();

    }

    public void eliminar(int i) {
        radar.quitar(i);
    }

    public void eliminarBuscando(String x) {
        radar.eliminar(x);
    }
}
