/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerdeprogramacion.views;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Ariel
 */
public class Radar extends Applet {

    public int Ancho = 900;
    public int Alto = 900;
    private int cantidadAvion = 0;
    private int cantidadAeropuerto = 0;
    public final Color bg = Color.BLACK;
    boolean thSuspendida = false;
    public int ind;
    Image im;
    Graphics offscreen;
    ArrayList<Graficador> lista = new ArrayList<>();

    public void init() {
        resize(Ancho, Alto);
        try {
            im = createImage(Ancho, Alto);
            offscreen = im.getGraphics();
        } catch (Exception e) {
            offscreen = null;
        }
    }

    private Color darColor(String color) {
        switch (color) {
            case "Amarillo":

                return Color.yellow;
            case "Azul":

                return Color.blue;
            case "Naranja":
                return Color.orange;
        }
        return Color.red;
    }

    public void anadir(int x, int y, int x2, int y2, String color, String tipo) {

        Color col = darColor(color);
        Graficador graficador = new NoEstaticos(this, 10, 1000, col, tipo);
        graficador.setIni(x, y);
        graficador.setDest(x2, y2);
        graficador.procesarFormula();
        lista.add(graficador);
        cantidadAvion ++;
    }
    public void anadir(int x, int y, String color, String tipo) {
        Color col = darColor(color);
        Graficador graficador = new Estaticos(this, 10, 1000, col, tipo);
        graficador.setIni(x, y);
        graficador.setDest(x, y);
        graficador.procesarFormula();
        lista.add(graficador);
        cantidadAeropuerto ++;
    }


    public int getCantidadAvion() {
        return cantidadAvion;
    }

    public int getCantidadAeropuerto() {
        return cantidadAeropuerto;
    }

    public void start() {
        for (Graficador objeto : lista) {
            objeto.start();
        }
        repaint();
    }

    public void stop() {
        for (Graficador objeto : lista) {
            objeto.stop();
        }
    }

    public void update(Graphics g) {
        paint(g);

    }

    public void paint(Graphics g) {
        if (offscreen != null) {
            paintApplet(offscreen);
            g.drawImage(im, 0, 0, this);
        } else {
            paintApplet(g);
        }
    }

    public void paintApplet(Graphics g) {
        g.setColor(Color.white);
        g.fillRect(0, 0, Ancho, Alto);
        for (Graficador objeto : lista) {
            objeto.paint(g);
        }
    }

    public boolean mouseDown(Event evt, int x, int y) {
        for (Graficador coordinador : lista) {
            if (thSuspendida) {
                coordinador.thread.resume();
            } else {
                coordinador.thread.suspend();
            }
            thSuspendida = !thSuspendida;
        }
        return (true);
    }

    public boolean quitar(int o) {
        return lista.remove(o) == null ? false : true;
    }

    public boolean eliminar(String x) {
        int i = 0;
        int j = lista.size();
        while (!lista.get(i).equals(x) && !lista.get(j).equals(j)) {
            i++;
            j--;
        }
        boolean flag = false;
        if (lista.get(i).getTipo().equals(x)) {
            flag = lista.remove(i) == null;
        }
        if (lista.get(j).getTipo().equals(x)) {
            flag = lista.remove(j) == null;
        }
        return flag;
    }

    public ArrayList<Graficador> getLista() {
        return lista;
    }
}
