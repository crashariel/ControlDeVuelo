/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerdeprogramacion.views;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedHashMap;
import java.util.Map;
import tallerdeprogramacion.FormulaRecta;

/**
 *
 * @author Ariel
 */
public abstract class Graficador implements Runnable {

    static int threadNum = 1;
    protected Color color = null;
    protected int ox;
    protected int oy;

    protected Graphics graphics;
    protected int naptime;
    protected int desX;
    protected int desY;
    protected int tamano;
    protected Thread thread = null;
    protected Map<Integer, Integer> coorEscala;
    protected Radar applet;
    protected FormulaRecta formula;
    protected String tipo;
    protected int accion;
    protected boolean estado;

    public Graficador(Radar applet, int tamano, int naptime, Color color, String tipo) {
        coorEscala = new LinkedHashMap<>();
        estado = false;
        this.tipo = tipo;
        this.applet = applet;
        this.tamano = tamano;
        this.naptime = naptime;
        this.color = color;
        accion = 1;
        if (color == null) {
            color = Color.white;
        }
    }

    public Radar getApplet() {
        return applet;
    }

    public int getOx() {
        return ox;
    }

    public Color getColor() {
        return color;
    }

    public int getOy() {
        return oy;
    }

    public int getDesX() {
        return desX;
    }

    public int getDesY() {
        return desY;
    }

    public void setIni(int x, int y) {
        ox = x;
        oy = y;
    }

    public void setDest(int x, int y) {
        desX = x;
        desY = y;
    }

    protected void getTipo(String st) {
        tipo = st;
    }
    
    public void darEscala(int x, int y){
        coorEscala.put(x, y);
    }

    public void procesarFormula() {
        formula = new FormulaRecta(ox, oy, desX, desY);
    }

    public void run() {
        thread.setPriority(Thread.MAX_PRIORITY);
        while (thread != null) {
        }
        thread = null;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, Integer.toString(threadNum++));
            thread.start();
        }
    }

    public void stop() {
        thread = null;
    }

    public abstract void paint(Graphics g);

    public String getTipo() {
        return tipo;
    }

    public abstract String toString();

    public int getAccion() {
        return accion;
    }

    public void sigAccion() {
        accion++;
    }

    public int getDistancia() {
        return ox;
    }

    public int getAltura() {
        return oy;
    }

    public void setDistancia(int distancia) {
        this.ox = distancia;
    }

    public void aumentarAltura(int razon) {
        oy += razon;
    }

    public void reducirAltura(int razon) {
        oy -= razon;
    }
}
