package tallerdeprogramacion;


public class FormulaRecta
{
    private double constante1;
    private double constante2;
    private int aux;
    public FormulaRecta(double x,double y,double dX,double dY)
    {
        definir(x,y,dX,dY);   
        aux=calDistancia(x,y,dX,dY);
    }
    
    private void definir(double x,double y,double Dx,double Dy)
    {
        if(Dx-x!=0)
        {
            constante1=(Dy-y)/(Dx-x);
            definirCons2(x,y);
        }
        else
        {
           constante1=constante2=1;
        }
    }
    
    private void definirCons2(double x,double y)
    {
        constante2=y-constante1*x;
    }
    
    public double devolverY(double x)
    {
        double salida=0;
        salida=constante1*x+constante2;
        return salida;
    }
    
    public static int calDistancia(double x,double y,double Dx,double Dy)
    {
        return (int)(Math.sqrt((Math.pow(x-Dx,2))+(Math.pow(y-Dy,2))));
    }
    
    public int getDistancia()
    {
        return aux;
    }
    /*
    public String formula()
    {
        
    }*/
}
